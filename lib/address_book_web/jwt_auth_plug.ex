defmodule AddressBookWeb.JwtAuthPlug do
  import Plug.Conn

  def init(_) do
  end

  def call(conn, _opts) do
    case authenticate(conn) do
      {:ok, claims} ->
        success(conn, claims)
      {:error, _error} ->
        forbidden(conn)
    end
  end

  # Internal functions

  def authenticate(conn) do
    case auth_enabled?() do
      true ->
        verify_token(conn)
      false ->
        {:ok, %{"sub" => %{"email" => "", "id" => ""}}}
    end
  end

  defp verify_token(conn) do
    with {:ok, token} <- extract_token(conn),
         %JOSE.JWK{} = pub_key <- JOSE.JWK.from_pem(public_key()),
         {true, claims, _} <- JOSE.JWK.verify(token, pub_key) do
      Jason.decode(claims)
    end
  end

  defp extract_token(conn) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] ->
        {:ok, token}

      _ ->
        {:error, :auth_token_missing}
    end
  end

  defp success(conn, claims) do
    assign(conn, :claims, claims)
  end

  defp forbidden(conn) do
    conn
    |> put_status(:unauthorized)
    |> Phoenix.Controller.render(AddressBookWeb.ErrorView, "401.html")
    |> halt()
  end

  defp public_key() do
    Application.get_env(:address_book, :public_key)
  end

  def auth_enabled?() do
    Application.get_env(:address_book, :auth_enabled) != 0
  end
end
