defmodule AddressBookWeb.ContactController do
  use AddressBookWeb, :controller

  alias AddressBook.Directories
  alias AddressBook.Directories.Contact

  def index(conn, %{"case_id" => case_id}) do
    contacts = Directories.list_contacts(case_id)
    render(conn, "index.html", contacts: contacts, case_id: case_id)
  end

  def new(conn, %{"case_id" => case_id}) do
    changeset = Directories.change_contact(%Contact{})
    render(conn, "new.html", changeset: changeset, case_id: case_id)
  end

  def create(conn, %{"case_id" => case_id, "contact" => contact_params}) do
    params = Map.put(contact_params, "case_id", case_id)

    case Directories.create_contact(params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact created successfully.")
        |> redirect(to: Routes.case_contact_path(conn, :show, case_id, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, case_id: case_id)
    end
  end

  def show(conn, %{"case_id" => case_id, "id" => id}) do
    contact = Directories.get_contact!(id)
    render(conn, "show.html", contact: contact, case_id: case_id)
  end

  def edit(conn, %{"case_id" => case_id, "id" => id}) do
    contact = Directories.get_contact!(id)
    changeset = Directories.change_contact(contact)
    render(conn, "edit.html", contact: contact, changeset: changeset, case_id: case_id)
  end

  def update(conn, %{"case_id" => case_id, "id" => id, "contact" => contact_params}) do
    contact = Directories.get_contact!(id)

    case Directories.update_contact(contact, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact updated successfully.")
        |> redirect(to: Routes.case_contact_path(conn, :show, case_id, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact: contact, changeset: changeset, case_id: case_id)
    end
  end

  def confirm_delete(conn, %{"case_id" => case_id, "id" => id}) do
    contact = Directories.get_contact!(id)
    render(conn, "confirm.html", contact_id: contact.id, case_id: case_id)
  end

  def delete(conn, %{"case_id" => case_id, "id" => id, "confirm" => "yes"}) do
    contact = Directories.get_contact!(id)
    {:ok, _contact} = Directories.delete_contact(contact)

    conn
    |> put_flash(:info, "Contact deleted successfully.")
    |> redirect(to: Routes.case_contact_path(conn, :index, case_id))
  end

  def delete(conn, %{"case_id" => case_id}) do
    redirect(conn, to: Routes.case_contact_path(conn, :index, case_id))
  end
end
