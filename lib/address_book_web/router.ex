defmodule AddressBookWeb.Router do
  use AddressBookWeb, :router

  alias AddressBookWeb.JwtAuthPlug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug JwtAuthPlug
  end

  scope "/", AddressBookWeb do
    pipe_through :browser

    resources "/cases", CaseController do
      resources "/contacts", ContactController
      get "/contacts/:id/delete", ContactController, :confirm_delete
    end

    get "/", PageController, :index
  end
end
