defmodule AddressBook.Directories.Contact do
  use Ecto.Schema
  import Ecto.Changeset

  @required_attrs [:case_id]
  @valid_attrs [:title, :first_name, :last_name, :mobile_phone, :address]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "contacts" do
    field :address, :string
    field :case_id, Ecto.UUID
    field :first_name, :string
    field :last_name, :string
    field :mobile_phone, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, @required_attrs ++ @valid_attrs)
    |> validate_required([:case_id])
  end
end
