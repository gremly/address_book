# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :address_book,
  ecto_repos: [AddressBook.Repo],
  generators: [binary_id: true],
  auth_enabled: String.to_integer(System.get_env("ENABLE_AUTHENTICATION") || "1"),
  public_key: System.get_env("RSA_PUBLIC_KEY")

# Configures the endpoint
config :address_book, AddressBookWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MwgwAKhAFRPHasRIxUnwQnWazS9Sod7SHF8e1eM+z310T2rRig0jxbsv8fCU1LmM",
  render_errors: [view: AddressBookWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: AddressBook.PubSub,
  live_view: [signing_salt: "GaBFbZad"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
