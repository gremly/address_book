use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :address_book, AddressBook.Repo,
  username: "devel",
  password: "devel",
  database: "address_book_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :address_book, AddressBookWeb.Endpoint,
  http: [port: 4002],
  server: false

config :address_book,
  auth_enabled: 0

# Print only warnings and errors during test
config :logger, level: :warn
