defmodule AddressBookWeb.AuthPlugTest do
  use ExUnit.Case
  use Plug.Test

  alias AddressBookWeb.JwtAuthPlug

  @token "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJwYXJjLWNsaSIsInN1YiI6eyJpZCI6IjQyIiwiZW1haWwiOiJ0ZXN0QGJldHRlcmRvYy5kZSJ9LCJpYXQiOjE2MjMyNDc2ODcsImV4cCI6MTYyMzI0NzgwN30.IkGssqKrGvU9PB96DGj4BNg3HtflD8f2ND2gsTUmrb6M53KuiEaOrk1_krFLDQnt48eRr_I3xOpIKYDzbquNcUR0LmxXcBuPI_VBHTMMj_vu8rr-A15kffB8ckT6gwX9CAlJBRy6UjcisvDv-bQQMCFmk8JEapMgzc8SIyOAUKyd_FAJ5HmpPzOpB7WLf6Tu9nVFGrMZ1h_kYN--poB02wqhhn6Tgz86yk4Uvb04mv_7XVC-G8zgo6-5EL2ULtzzbEUchBLTyi7IRRtbeOmJKL4f-8aY4qv3V_2EgBgkdL_vwmf8-xczoNBYdECnIbyIYIf_0Qys70mS6il5wuM2QQ"


  test "returns welcome" do
    conn =
      :get
      |> conn("/private-route")
      |> put_req_header("authorization", "Bearer #{@token}")
      |> JwtAuthPlug.call([])

    assert %{claims: %{}} = conn.assigns
  end
end
