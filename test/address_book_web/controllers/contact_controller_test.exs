defmodule AddressBookWeb.ContactControllerTest do
  use AddressBookWeb.ConnCase

  alias AddressBook.Directories

  @case_id "7488a646-e31f-11e4-aace-600308960662"

  @create_attrs %{
    address: "some address",
    first_name: "some first_name",
    last_name: "some last_name",
    mobile_phone: "some mobile_phone",
    title: "some title"
  }

  @update_attrs %{
    address: "some updated address",
    first_name: "some updated first_name",
    last_name: "some updated last_name",
    mobile_phone: "some updated mobile_phone",
    title: "some updated title"
  }

  def fixture(:contact) do
    {:ok, contact} =
      @create_attrs
      |> Map.put(:case_id, @case_id)
      |> Directories.create_contact()

    contact
  end

  describe "index" do
    test "lists all contacts", %{conn: conn} do
      conn = get(conn, Routes.case_contact_path(conn, :index, @case_id))
      assert html_response(conn, 200) =~ "Listing Contacts"
    end
  end

  describe "new contact" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.case_contact_path(conn, :new, @case_id))
      assert html_response(conn, 200) =~ "New Contact"
    end
  end

  describe "create contact" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.case_contact_path(conn, :create, @case_id), contact: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.case_contact_path(conn, :show, @case_id, id)

      conn = get(conn, Routes.case_contact_path(conn, :show, @case_id, id))
      assert html_response(conn, 200) =~ "Show Contact"
    end
  end

  describe "edit contact" do
    setup [:create_contact]

    test "renders form for editing chosen contact", %{conn: conn, contact: contact} do
      conn = get(conn, Routes.case_contact_path(conn, :edit, @case_id, contact))
      assert html_response(conn, 200) =~ "Edit Contact"
    end
  end

  describe "update contact" do
    setup [:create_contact]

    test "redirects when data is valid", %{conn: conn, contact: contact} do
      conn =
        put(conn, Routes.case_contact_path(conn, :update, @case_id, contact),
          contact: @update_attrs
        )

      assert redirected_to(conn) == Routes.case_contact_path(conn, :show, @case_id, contact)

      conn = get(conn, Routes.case_contact_path(conn, :show, @case_id, contact))
      assert html_response(conn, 200) =~ "some updated address"
    end
  end

  describe "delete contact" do
    setup [:create_contact]

    test "renders confirmation for deleting chosen contact", %{conn: conn, contact: contact} do
      conn = get(conn, Routes.case_contact_path(conn, :confirm_delete, @case_id, contact.id))
      assert html_response(conn, 200) =~ "Delete Contact"
    end

    test "redirects to index for unconfirmed delete", %{conn: conn, contact: contact} do
      conn =
        delete(conn, Routes.case_contact_path(conn, :delete, @case_id, contact), %{confirm: "no"})

      assert redirected_to(conn) == Routes.case_contact_path(conn, :index, @case_id)

      conn = get(conn, Routes.case_contact_path(conn, :show, @case_id, contact))
      assert html_response(conn, 200) =~ "some address"
    end

    test "deletes chosen contact", %{conn: conn, contact: contact} do
      conn =
        delete(conn, Routes.case_contact_path(conn, :delete, @case_id, contact), %{confirm: "yes"})

      assert redirected_to(conn) == Routes.case_contact_path(conn, :index, @case_id)

      assert_error_sent 404, fn ->
        get(conn, Routes.case_contact_path(conn, :show, @case_id, contact))
      end
    end
  end

  defp create_contact(_) do
    contact = fixture(:contact)
    %{contact: contact}
  end
end
