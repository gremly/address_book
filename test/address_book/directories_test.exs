defmodule AddressBook.DirectoriesTest do
  use AddressBook.DataCase

  alias AddressBook.Directories

  @case_id "7488a646-e31f-11e4-aace-600308960662"

  describe "contacts" do
    alias AddressBook.Directories.Contact

    @valid_attrs %{
      address: "some address",
      case_id: @case_id,
      first_name: "some first_name",
      last_name: "some last_name",
      mobile_phone: "some mobile_phone",
      title: "some title"
    }
    @update_attrs %{
      address: "some updated address",
      case_id: @case_id,
      first_name: "some updated first_name",
      last_name: "some updated last_name",
      mobile_phone: "some updated mobile_phone",
      title: "some updated title"
    }

    @invalid_attrs %{case_id: nil}

    def contact_fixture(attrs \\ %{}) do
      {:ok, contact} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directories.create_contact()

      contact
    end

    test "list_contacts/1 returns all contacts for the given case id" do
      contact = contact_fixture()
      assert Directories.list_contacts(@case_id) == [contact]
    end

    test "get_contact!/2 returns the contact with given id" do
      contact = contact_fixture()
      assert Directories.get_contact!(contact.id) == contact
    end

    test "create_contact/1 with valid data creates a contact" do
      assert {:ok, %Contact{} = contact} = Directories.create_contact(@valid_attrs)
      assert contact.address == "some address"
      assert contact.case_id == "7488a646-e31f-11e4-aace-600308960662"
      assert contact.first_name == "some first_name"
      assert contact.last_name == "some last_name"
      assert contact.mobile_phone == "some mobile_phone"
      assert contact.title == "some title"
    end

    test "create_contact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directories.create_contact(@invalid_attrs)
    end

    test "update_contact/2 with valid data updates the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{} = contact} = Directories.update_contact(contact, @update_attrs)
      assert contact.address == "some updated address"
      assert contact.case_id == @case_id
      assert contact.first_name == "some updated first_name"
      assert contact.last_name == "some updated last_name"
      assert contact.mobile_phone == "some updated mobile_phone"
      assert contact.title == "some updated title"
    end

    test "update_contact/2 with invalid data returns error changeset" do
      contact = contact_fixture()
      assert {:error, %Ecto.Changeset{}} = Directories.update_contact(contact, @invalid_attrs)
      assert contact == Directories.get_contact!(contact.id)
    end

    test "delete_contact/1 deletes the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{}} = Directories.delete_contact(contact)
      assert_raise Ecto.NoResultsError, fn -> Directories.get_contact!(contact.id) end
    end

    test "change_contact/1 returns a contact changeset" do
      contact = contact_fixture()
      assert %Ecto.Changeset{} = Directories.change_contact(contact)
    end
  end
end
