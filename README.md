# AddressBook

## Deploy

- Define the required enviroment variables:
  - DATABASE_URL
  - SECRET_KEY_BASE
  - RSA_PUBLIC_KEY

- Generate an estandar Elixir release using `mix release`.
- The application can be started using `address_book/bin/address_book daemon` inside
  of the generated release directory.
- By default the application listens to the port `4000`, once it is running it can be
  accessed at `http://localhost:4000`

