defmodule AddressBook.Repo.Migrations.CreateContacts do
  use Ecto.Migration

  def change do
    create table(:contacts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string
      add :first_name, :string
      add :last_name, :string
      add :mobile_phone, :string
      add :address, :string
      add :case_id, :uuid

      timestamps()
    end
  end
end
